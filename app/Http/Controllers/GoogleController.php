<?php

namespace App\Http\Controllers;
use Storage;
use Illuminate\Http\Request;

class GoogleController extends Controller
{
    public function uploadFileToGoogle(Request $request){     
        $file = $request->file('file');
        $name = $file->getClientOriginalName();       
        $id =  Storage::cloud()->putFileAs(env('GOOGLE_DRIVE_FOLDER_ID'), $file,$name);
        return $id;
    }
}
